/*class Saluda {
    constructor(name) {
        this.name = name;
    }

    diHola() {
        console.log("Hola " + this.name);
    }

    preguntaNombre = (name) => this.name = name;
}

const saluda = new Saluda('Sheyla');
saluda.diHola();

saluda.preguntaNombre("Sheyla Urbina");
saluda.diHola();*/

import sumFn from "./sum";​
class Factory {
    constructor(data) {
        this.data = data;
    }​
    sum() {
            return sumFn(this.data);
        }​
        /*res() {
            return function() {
                return this.data.a - this.data.b;
            };
        }​
        mul() {
            return function() {
                return this.data.a * this.data.b;
            };
        }​
        div() {
            return function() {
                return this.data.a / this.data.b;
            };
        }*/
}​
const factory = new Factory({ a: 2, b: 2 });​
const sum = factory.sum();
/*const res = factory.res();
const mul = factory.mul();
const div = factory.div();​*/
console.log(sum());
/*console.log(res());
console.log(mul());
console.log(div());*/