/*const sumar = require('./sumar.js')
const restar = require('./resta.js')
const multiplicar = require('./multiplicar.js')
const dividir = require('./dividir.js')*/

import sumar from './sumar.js';
import restar from './resta.js';
import multiplicar from './multiplicar.js';
import dividir from './dividir.js';

function factory(op, data) {
    if (op === "+") {
        return function() {
            return sumar(data.a, data.b);
        };
    }

    if (op === "-") {
        return function() {
            return restar(data.a, data.b);
        };
    }

    if (op === "*") {
        return function() {
            return multiplicar(data.a, data.b);
        };
    }

    if (op === "/") {
        return function() {
            return dividir(data.a, data.b);
        };
    }
}

const sum = factory("+", { a: 3, b: 2 });
const res = factory("-", { a: 10, b: 2 });
const mul = factory("*", { a: 5, b: 2 });
const div = factory("/", { a: 10, b: 2 });

console.log(sum());
console.log(res());
console.log(mul());
console.log(div());