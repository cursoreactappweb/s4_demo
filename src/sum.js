export default function(data) {
    return function() {
        return data.a + data.b;
    };
}